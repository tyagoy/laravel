<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PessoaEndereco extends Model
{
    const CREATED_AT = 'data_cadastro';
    const UPDATED_AT = 'data_alteracao';

    public function pessoa()
    {
        return $this->belongsTo('App\Pessoa');
    }
}
