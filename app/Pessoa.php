<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    const CREATED_AT = 'data_cadastro';
    const UPDATED_AT = 'data_alteracao';

    public function pessoaEnderecos()
    {
        return $this->hasMany('App\PessoaEndereco', 'cod_pessoa');
    }
}
