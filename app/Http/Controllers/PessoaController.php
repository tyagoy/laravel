<?php

namespace App\Http\Controllers;

use App\Pessoa;
use App\PessoaEndereco;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PessoaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pessoas = Pessoa::select('*')
            ->where('nome', 'like', "%".$request->nome."%")
            ->where('telefone', 'like', "%".$request->telefone."%")
            ->orderBy('data_alteracao', 'DESC')
            ->paginate(5);    

        return view('pessoas.index', [

            'nome'     => $request->nome,
            'telefone' => $request->telefone,
            'pessoas'  => $pessoas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pessoas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pessoa = new Pessoa;

        $pessoa->nome            = $request->nome;
        $pessoa->email           = $request->email;
        $pessoa->cpf             = $request->cpf;
        $pessoa->telefone        = $request->telefone;
        $pessoa->data_nascimento = $request->data_nascimento;

        $pessoa->save();

        foreach ($request->pessoa_enderecos as $endereco) {           

            $pessoaEndereco = new PessoaEndereco;

            $pessoaEndereco->endereco        = $endereco['endereco'];
            $pessoaEndereco->numero_endereco = $endereco['numero_endereco'];
            $pessoaEndereco->bairro          = $endereco['bairro'];
            $pessoaEndereco->cidade          = $endereco['cidade'];
            $pessoaEndereco->uf              = $endereco['uf'];
            $pessoaEndereco->complemento     = $endereco['complemento'];

            $pessoa->pessoaEnderecos()->save($pessoaEndereco);
        }

        return response()->json($pessoa->toJson());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pessoa  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function show(Pessoa $pessoa)
    {
        return response()->json($pessoa->load('pessoaEnderecos')->toJson());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pessoa  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function edit(Pessoa $pessoa)
    {
        return view('pessoas.edit', ['id' => $pessoa->id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pessoa  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pessoa $pessoa)
    {        
        $pessoa->nome            = $request->nome;
        $pessoa->email           = $request->email;
        $pessoa->cpf             = $request->cpf;
        $pessoa->telefone        = $request->telefone;
        $pessoa->data_nascimento = $request->data_nascimento;

        $pessoa->save();

        foreach ($request->pessoa_enderecos as $pessoa_endereco) {
             
            if(!isset($pessoa_endereco['id'])){

                $newPessoaEndereco = new PessoaEndereco;

                $newPessoaEndereco->endereco        = $pessoa_endereco['endereco'];
                $newPessoaEndereco->numero_endereco = $pessoa_endereco['numero_endereco'];
                $newPessoaEndereco->bairro          = $pessoa_endereco['bairro'];
                $newPessoaEndereco->cidade          = $pessoa_endereco['cidade'];
                $newPessoaEndereco->uf              = $pessoa_endereco['uf'];
                $newPessoaEndereco->complemento     = $pessoa_endereco['complemento'];

                $pessoa->pessoaEnderecos()->save($newPessoaEndereco);                
            }
        }

        foreach ($pessoa->pessoaEnderecos()->get() as $pessoaEndereco) {    

            $update = false;    

            foreach ($request->pessoa_enderecos as $pessoa_endereco) {                

                if(isset($pessoa_endereco['id'])){

                    if($pessoaEndereco->id == $pessoa_endereco['id']){

                        $pessoaEndereco->endereco        = $pessoa_endereco['endereco'];
                        $pessoaEndereco->numero_endereco = $pessoa_endereco['numero_endereco'];
                        $pessoaEndereco->bairro          = $pessoa_endereco['bairro'];
                        $pessoaEndereco->cidade          = $pessoa_endereco['cidade'];
                        $pessoaEndereco->uf              = $pessoa_endereco['uf'];
                        $pessoaEndereco->complemento     = $pessoa_endereco['complemento'];

                        $pessoaEndereco->save();                    

                        $update = true;
                    }                    

                }else{

                    $update = true;
                }
            }

            if($update == false){

                $pessoaEndereco->delete();  
            }
        }

        return response()->json($pessoa->toJson());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pessoa  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pessoa $pessoa)
    {
        foreach ($pessoa->pessoaEnderecos()->get() as $pessoaEndereco) {

            $pessoaEndereco->delete();
        }

        $pessoa->delete();

        return response()->json($pessoa->toJson());
    }
}
