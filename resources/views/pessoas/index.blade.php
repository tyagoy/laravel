<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>Laravel</title>
		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="/css/materialize.css" rel="stylesheet">
		<script src="/js/materialize.js"></script>
		<link href="/css/app.css" rel="stylesheet">		
	</head>
	<body>
		<div id="app">
			
			<nav>
				<div class="nav-wrapper deep-orange accent-2">
					<div class="container">
						<a href="#" class="brand-logo truncate">
							<i class="material-icons prefix">account_circle</i>
							Lista de clientes
						</a>
						<ul id="nav-mobile" class="right hide-on-med-and-down">
							<li>
								<a class="waves-effect waves-light" href="/pessoas/create">
									Adicionar
								</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			
			<main>				
				<div class="row">
					<form method="GET">
						<div class="input-field col s12 l6">
							<i class="material-icons prefix">account_circle</i>
							<input id="search_nome" type="text" class="validate" placeholder="Pesquisar por nome" name="nome" value="{{$nome}}">
							<label for="search_nome">Nome</label>
						</div>
						<div class="input-field col s12 l4">
							<i class="material-icons prefix">phone</i>
							<input id="search_telefone" type="text" class="validate" placeholder="Pesquisar por telefone" name="telefone" value="{{$telefone}}">
							<label for="search_telefone">Telefone</label>
						</div>
						<div class="s2 l2 center-align">
							<button class="waves-effect waves-light btn-large blue">	
								Procurar
							</button>						
						</div>
					</form>
				</div>
				<div class="row oa">
					<div class="col s12 m12 l12">
						<table>
							<thead>
								<tr>
									<th>Nome</th>
									<th>CPF</th>
									<th>Data de nascimento</th>
									<th>Telefone</th>
									<th>E-mail</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($pessoas as $pessoa)
								<tr>
									<td>{{$pessoa->nome}}</td>
									<td>{{$pessoa->cpf}}</td>
									<td>{{$pessoa->data_nascimento}}</td>
									<td>{{$pessoa->telefone}}</td>
									<td>{{$pessoa->email}}</td>
									<td>
										<a class="btn btn-small waves-effect waves-light red" @click="clickDelete({{$pessoa->id}})">
											<i class="material-icons">delete</i>
										</a>
										<a class="btn btn-small waves-effect waves-light blue" href="/pessoas/{{$pessoa->id}}/edit">
											<i class="material-icons">edit</i>
										</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>				
				</div>	
			</main>
			
			<footer class="page-footer">          		
            	{{ $pessoas->links() }}	            	
            </footer>			
			
		</div>	
	</body>
	<script src="/js/app.js"></script>
</html>