<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePessoaEnderecosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoa_enderecos', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('endereco', 60);
            $table->string('numero_endereco', 15);
            $table->string('complemento', 15);
            $table->string('bairro', 35);
            $table->string('cidade', 45);
            $table->string('uf', 2);
            $table->date('data_cadastro');
            $table->date('data_alteracao');
            $table->bigInteger('cod_pessoa');                            
            $table->index('cod_pessoa');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoa_enderecos');
    }
}
