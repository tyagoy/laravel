<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePessoasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoas', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('nome', 60);
            $table->string('cpf', 11);
            $table->date('data_nascimento');
            $table->string('telefone', 12);
            $table->string('email', 45);
            $table->date('data_cadastro');
            $table->date('data_alteracao');                          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoas');
    }
}
